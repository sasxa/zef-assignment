# Requirements

* [ ] slide groups
* [ ] individual slides
* [ ] layout controled by router
* [ ] content controlled by data

# Configuration
In `tsconfig.json` add alias for `shared/` folder:
```
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "@shared": "src/app/shared"
    },
    ...
  }
```

# Architecture
* Reactive immutable state:
```
export interface Slide {
  key: string;
  group: string;
  ...
}

export interface AppState {
  readonly route: {
    url: string;
    key: string;
  };
  readonly slides: Slide[];
}

state$: BehaviourSubject<AppState> = new BehaviourSubject({
  route: { url: 'build', key: '' },
  slides: [] // get slides from local storage maybe ??
})
```
* State changes:
```
onRouteChange() {
  url = RouterStateSnapshot.url
  key = ActivatedRoute.params.key
  state = state$.value
  route = { url, key }

  state$.next({ route, slides: state.slides })
}

// functional-style; easier to test
updateRoute(route, subject, state) {
  subject.next({ route, slides: state.slides })
}

onDataChange() {
  state = state$.value
  slides = state.slides.map(slide => ...)

  state$.next({ route: state.route, slides })
}

// functional-style; easier to test
updateSlides(slides, subject, state) {
  subject.next({ route: state.route, slides })
}
```

## Modules

`app.module`:
```
@NgModule({
  imports: [
    AppRoutingModule,
    SharedModule, // exports RouterModule
  ],
  declarations: [ AppComponent ],
  bootstrap: [ AppComponent ],
  providers: [
    StateService
  ],
})
```

## Routing

`app-routing.module`
```
export const routes: Routes = [
  { path: 'build',  loadChildren: './+build/build.module#BuildModule' },
  { path: 'design', loadChildren: './+design/design.module#DesignModule' },
  { path: 'setup',  loadChildren: './+setup/setup.module#SetupModule' },
];
```

## State
Services do all the work. *** minimal dependencies

`@shared/state.service.ts`
```
export class StateService() {
  // Exception to the code standard rule, because of the hoisting ??
  private state$: BehaviourSubject<AppState>

  public route$ = state$.pipe(
    map(state => state.route),
    share(),
  ).do(console.log); // TODO: check if this is called twice or actually shared...

  public slides$ = state$.pipe(
    map(state => state.slides),
    share(),
  );

  constructor(private router: Router) { this.init(); }

  private get route() {
    const { root, url } = this.router.routerState;
    const activated =  traverse(root, o => o.firstChild, o => !Boolean(o.firstChild));

    return { url, key: activated.params.key };
  }

  private updateRoute(route, subject = this.state$) {
    const slides = subject.value.slides;
    subject.next({ route, slides });
  }

  private init() {
    this.router.events.forEach(event =>
      event instanceof NavigationEnd
        ? this.updateRoute(this.route)
        : noop);
  }
}
```

Components pass commands between templates to the services.

`app.component.ts`
```
export class AppComponent { 
  constructor(public state: StateService) { }
}
```

## Layout

`app.component.html`:
```
<header>
  ZEF Assignment

<main>
  <aside>
    <tabs>
      <router-outlet></router-outlet>
      <ng-component>

  <section>
    <editor>
})
```

# APIs

## Collection 
Reusable component which handles display and grouping of items 
[proof of concept](https://stackblitz.com/edit/angular-7tdzxr?file=app/app.component.ts).
It should be responsibe for re-arranging items (drag-and-drop).

`collection/collection-item.model.ts`
```
export interface CollectionItem {
  key: string;
  group: string;
  order?: number;
}
```

`collection/collection.component.ts`
```
export class CollectionComponent { 

  @Input() groupTemplate: TemplateRef<any>;
  @Input() itemTemplate: TemplateRef<any>;
  @Input() data: CollectionItem[];

  get items() {
    return data.filter(item => !this.inGroup(item))
  }

  ctx(item: CollectionItem) {
    return {
      $implicit: item
      children: this.isGroup(item)
        ? data.filter(ci => ci.group === item.key)
        : null
    }
  }

  inGroup(item: CollectionItem) {
    return !!item.group ;
  }

  isGroup(item: CollectionItem) {
    return item.type === 'group';
  }

  add(item: CollectionItem) {
    return [ item, ...this.data];
  }

  remove(item: CollectionItem) {
    return this.data
      .filter(ci => 
        ci.key !== item.key 
        && ci.group !== item.key);
  }
}
```

`collection/collection.component.html`
```
<ng-content select="header">

<div class="wrapper">
  <ng-container *ngFor="let item of items">
    <ng-container *ngTemplateOutlet="isGroup(item) ? groupTemplate : itemTemplate; context: ctx;">

<ng-content select="footer">
```

`editor/editor.component.html`
```
<collection [groupTemplate]="group" [itemTemplate]="single" [data]="state.slides$ |async">
  <div header>
    ZEF Assignment
  <div footer>
    <add-question>
    <add-group>

<ng-template #group let-item let-children="children">
  <div header>
    <slide-editor-component [data]="item">
  <div children>
    <slide-editor-component *ngFor="let child of children" [data]="child">
  <div footer>
    <add-question-to-group>

<ng-template #single let-item>
  <slide-editor-component [data]="item">
```

`menu/menu.component.html`
```
<collection [groupTemplate]="group" [itemTemplate]="single" [data]="state.slides$ |async">

<ng-template #group let-item>
  <group-menu-component [data]="item">
<ng-template #single let-item>
  <slide-menu-component [data]="item">
```
