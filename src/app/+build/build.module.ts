import { BuildComponent } from './build.component';
import { BuildRoutingModule } from './build-routing.module';
import { CommonModule } from '@angular/common';
import { DndModule } from 'ng2-dnd';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    DndModule.forRoot(),
    BuildRoutingModule,
    SharedModule,
  ],
  declarations: [ BuildComponent ]
})
export class BuildModule { }
