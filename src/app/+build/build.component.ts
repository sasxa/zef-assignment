import { Component, HostBinding } from '@angular/core';

import { Slide } from '../shared/state.interface';
import { StateService } from '../shared/state.service';
import { decOrder } from '../shared/utils';

@Component({
  selector: 'app-build',
  templateUrl: './build.component.html',
  styleUrls: [ './build.component.scss' ]
})
export class BuildComponent {
  @HostBinding('class') css = 'column-container';

  constructor(public state: StateService) { }

  addToGroup(value: Slide) {
    this.state.addSlide({ key: value.key });
  }

  appendGroup() {
    this.state.addSlide({ type: 'group' });
  }

  appendSlide() {
    this.state.addSlide({});
  }

  insertGroup(value: Slide) {
    this.state.addSlide({ type: 'group', order: decOrder(value.order) });
  }

  insertSlide(value: Slide) {
    this.state.addSlide({ order: decOrder(value.order) });
  }

  dropSlide(value: Slide, target?: Slide) {
    this.state.moveSlide(value, target);
  }

}
