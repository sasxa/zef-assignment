import { RouterModule, Routes } from '@angular/router';

import { BuildComponent } from './build.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', component: BuildComponent },
  { path: ':key', component: BuildComponent },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class BuildRoutingModule { }
