import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { BuildComponent } from './build.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { StateService } from '../shared/state.service';

describe('BuildComponent', () => {
  let component: BuildComponent;
  let fixture: ComponentFixture<BuildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: StateService, useValue: null }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
