import {
  ExtraOptions,
  PreloadAllModules,
  RouteReuseStrategy,
  RouterModule,
  Routes
} from '@angular/router';

import { Component } from '@angular/core';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  { path: 'build', loadChildren: './+build/build.module#BuildModule' },
  { path: 'design', loadChildren: './+design/design.module#DesignModule' },
  { path: 'setup', loadChildren: './+setup/setup.module#SetupModule' },
  { path: '**', redirectTo: 'build' },
];

export const options: ExtraOptions = {
  preloadingStrategy: PreloadAllModules,
  enableTracing: false,
  initialNavigation: true,
};

@NgModule({
  exports: [ RouterModule ],
  imports: [
    RouterModule.forRoot(routes, options),
  ],
})
export class AppRoutingModule { }
