import { RouterModule, Routes } from '@angular/router';

import { DesignComponent } from './design.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', component: DesignComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesignRoutingModule { }
