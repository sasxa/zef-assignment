import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { EditorModule } from './editor/editor.module';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { StateService } from './shared/state.service';

@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [ AppComponent ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    EditorModule,
    SharedModule,
  ],
  providers: [
    StateService,
  ],
})
export class AppModule { }
