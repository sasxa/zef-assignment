import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { SwiperConfigInterface, SwiperDirective } from 'ngx-swiper-wrapper';
import { map, tap } from 'rxjs/operators';

import { CollectionComponent } from '../shared/collection/collection.component';
import { Slide } from '../shared/state.interface';
import { StateService } from '../shared/state.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'editor',
  templateUrl: './editor.component.html',
  styleUrls: [ './editor.component.scss' ]
})
export class EditorComponent implements AfterViewInit {
  @Input() data;

  @ViewChild(SwiperDirective)
  private swiper: SwiperDirective;

  constructor(public state: StateService) { }

  ngAfterViewInit() {
    this.state.route$.pipe(
      map(route => route.key),
      map(key =>
        this.data.findIndex(item => item.key === key))

    ).forEach(index => {
      if (this.swiper.getIndex() !== index) {
        this.swiper.setIndex(index);
      }
    });
  }

  addToGroup(value: Slide) {
    this.state.addSlide({ key: value.key });
  }

  appendSlide() {
    this.state.addSlide({});
  }

  updateRoute(index: number) {
    const slide = this.data[ index ];
    if (slide) {
      this.state.navigate(`/build/${ slide.key }`);
    }
  }

}
