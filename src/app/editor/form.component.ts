import { Component, Input, OnInit } from '@angular/core';

import { Slide } from '../shared/state.interface';
import { StateService } from '../shared/state.service';

@Component({
  selector: 'editor-form',
  templateUrl: './form.component.html',
  styleUrls: [ './form.component.scss' ]
})
export class FormComponent {
  @Input() data: Slide;

  constructor(public state: StateService) { }

  removeSlide() {
    this.state.removeSlide(this.data);
  }

  updateSlide(value: Partial<Slide>) {
    this.state.updateSlide(this.data.key, {
      ...this.data, ...value
    });
  }

}
