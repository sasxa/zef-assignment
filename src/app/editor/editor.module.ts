import { SWIPER_CONFIG, SwiperConfigInterface, SwiperModule } from 'ngx-swiper-wrapper';

import { CommonModule } from '@angular/common';
import { EditorComponent } from './editor.component';
import { FormComponent } from './form.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'vertical',
  slidesPerView: 1,
  centeredSlides: true,
  observer: true,
  keyboard: true,
  mousewheel: true,
  scrollbar: true,
  // autoHeight: true,
};

@NgModule({
  declarations: [
    EditorComponent,
    FormComponent,
  ],
  exports: [
    EditorComponent,
    FormComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    SwiperModule,
  ],
  providers: [
    { provide: SWIPER_CONFIG, useValue: DEFAULT_SWIPER_CONFIG }
  ]
})
export class EditorModule { }
