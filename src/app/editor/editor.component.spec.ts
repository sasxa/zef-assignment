import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { EditorComponent } from './editor.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { StateService } from '../shared/state.service';

describe('EditorComponent', () => {
  let component: EditorComponent;
  let fixture: ComponentFixture<EditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: StateService, useValue: null }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
