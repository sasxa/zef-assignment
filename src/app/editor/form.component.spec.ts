import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { FormComponent } from './form.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { StateService } from '../shared/state.service';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: StateService, useValue: null }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
