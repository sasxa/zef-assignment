import * as sampleData from '../../../sample.json';

import { AppState, Slide } from './state.interface';
import { NavigationEnd, Router } from '@angular/router';
import { debug, decOrder, incOrder, noop, randomKey, reorder, sort, traverse } from './utils';
import { filter, map, share, tap } from 'rxjs/operators';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

interface BackendService {
  create: (...x) => void;
  read: (...x) => Observable<any>;
  update: (...x) => void;
  delete: (...x) => void;
}

@Injectable()
export class StateService {
  private state$: BehaviorSubject<AppState> = new BehaviorSubject({
    route: { url: 'build', key: '' },
    slides: reorder(sampleData as any) as any // get slides from local storage maybe ??
    // slides: []
  });

  // https://github.com/ReactiveX/rxjs/blob/master/doc/pipeable-operators.md
  private update$ = this.state$.pipe(
    // tap(state => debug(state)),
  );

  public route$ = this.update$.pipe(
    map(state => state.route),
    share(),
  );

  public slides$ = this.update$.pipe(
    map(state => state.slides),
  );

  public ui$ = this.update$.pipe(
    map(state => state.ui),
    filter(ui => !!ui),
  );

  public questions$ = this.slides$.pipe(
    map(slides =>
      slides.filter(item => item.type === 'question')),
  );

  public groups$ = this.slides$.pipe(
    map(slides =>
      slides.filter(item => item.type === 'group')),
  );

  public popup$ = this.ui$.pipe(
    map(ui => ui.popup),
    share(),
  );

  constructor(private router: Router) {
    this.init();
  }

  addSlide(value: Partial<Slide>) {
    /**
     * Add slide functionality:
     *
     * if data.key      -> Add to group
     * if not data.key  -> Add slide of data.type
     */
    const slide: Slide = {
      key: randomKey(),
      group: value.key || '',
      order: value.order || 1,
      title: '',
      type: value.type || 'question'
    };

    /**
     * Add X to group (3, 4, 5):
     *
     *   copy:     [1, 2, 3, 4, 5, 6, 7]
     *   reverse:  [7, 6, 5, 4, 3, 2, 1]
     *   reduce:   [7, 6, X, 5, X, 4, X, 3, 2, 1]
     *   filter:   [7, 6, X, 5, 4, 3, 2, 1]
     *   reverse:  [1, 2, 3, 4, 5, X, 6, 7]
     *
     */
    const slides
      = !this.state$.value.slides.length
        ? [ slide ]
        : [ ...this.state$.value.slides ]
          .reverse()
          .reduce((acc, item, index, array) => {
            return !value.key
              ? [ slide, ...array ]
              : (item.group === value.key || item.key === value.key)
                ? [ ...acc, { ...slide, order: incOrder(item.order) }, item ]
                : [ ...acc, item ];
          }, [])
          .filter((v, i, a) => a.findIndex(o => o.key === v.key) === i)
          .reverse()
          .sort(sort('asc', (item: Slide) => item.order));

    this.updateSlides(reorder(slides));
  }

  removeSlide(value: Slide) {
    const slides
      = this.state$.value.slides
        .filter(item =>
          item.key !== value.key
          && item.group !== value.key);

    this.updateSlides(reorder(slides));
  }

  updateSlide(key: string, value: Partial<Slide>) {
    const slides
      = this.state$.value.slides
        .map(item => item.key === key
          ? { ...item, ...value }
          : item);

    this.updateSlides(slides);
  }

  moveSlide(value: Slide, target?: Slide) {
    const moving
      = this.state$.value.slides
        .filter(item =>
          item.group === value.key
          || item.key === value.key);

    const fixed
      = this.state$.value.slides
        .filter(item => !moving.includes(item));

    const i = fixed.indexOf(target);

    const slides
      = target
        ? [
          ...fixed.slice(0, i),
          ...moving,
          ...fixed.slice(i) ]
        : fixed.concat(moving);

    this.updateSlides(reorder(slides));
  }

  updateUI(ui) {
    this.state$.next({ ...this.state$.value, ui });
  }

  navigate(url: string) {
    this.router.navigateByUrl(url);
  }

  loadSurvey(key: string, backend?: BackendService) {
    const fake_backend_response = of({
      'survey key': {
        'slide 1 key': { /* ...data */ },
        'slide 2 key': { /* ...data */ },
      }
    });

    const response
      = backend
        ? backend.read(key)
        : fake_backend_response;

    return response.pipe(
      map(data => Object.values(data)[ 0 ]),
      map(data => Object.values(data)),
      map(slides =>
        slides.sort(sort('asc', (item: Slide) => item.order))),

    ).forEach(items => this.updateSlides(items));
  }

  private get route() {
    const { root } = this.router.routerState;
    const { url } = this.router.routerState.snapshot;
    const activated
      = traverse(root, o => o.firstChild, o => !Boolean(o.firstChild));

    return { url, key: activated.params.value.key };
  }

  private updateRoute(route) {
    this.state$.next({ ...this.state$.value, route });
  }

  private updateSlides(slides) {
    this.state$.next({ ...this.state$.value, slides });
  }

  private init() {
    this.router.events.forEach(event =>
      event instanceof NavigationEnd
        ? this.updateRoute(this.route)
        : noop);
  }

}
