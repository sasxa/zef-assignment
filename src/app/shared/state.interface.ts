export interface Slide {
  key: string;
  group: string;
  title?: string;
  order: number;
  type: 'group' | 'question' | string;
}

interface UI {
  popup: string;
}

export interface AppState {
  readonly route: {
    url: string;
    key: string;
  };
  readonly slides: Slide[];
  readonly ui?: UI;
}
