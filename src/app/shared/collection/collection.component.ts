import { Component, Input, OnInit, TemplateRef, TrackByFunction } from '@angular/core';

export interface CollectionItem {
  key: string;
  group: string;
  order?: number;
  type: 'group' | 'question' | string;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'collection',
  templateUrl: './collection.component.html'
})
export class CollectionComponent {

  @Input() groupTemplate: TemplateRef<any>;
  @Input() itemTemplate: TemplateRef<any>;
  @Input() data: CollectionItem[];

  inGroup(value: CollectionItem) {
    return !!value.group;
  }

  isGroup(value: CollectionItem) {
    return value.type === 'group';
  }

  get items() {
    return this.data
      ? this.data.filter(item => !this.inGroup(item))
      : [];
  }

  ctx(value: CollectionItem) {
    const children
      = this.isGroup(value)
        ? this.data.filter(item => item.group === value.key)
        : null;

    const first
      = this.data.indexOf(value) === 0;

    const last
      = this.data.indexOf(value) === this.data.length - 1;

    return {
      $implicit: value,
      children,
      first,
      last,
    };
  }

  trackByKey(index: number, item: CollectionItem) {
    return item.key;
  }
}
