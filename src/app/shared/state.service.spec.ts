import { TestBed, inject } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { StateService } from './state.service';

describe('StateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [ StateService ]
    });
  });

  it('should be created', inject([ StateService ], (service: StateService) => {
    expect(service).toBeTruthy();
  }));
});
