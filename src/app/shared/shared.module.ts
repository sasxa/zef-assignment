import { CollectionComponent } from './collection/collection.component';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HoverActionComponent } from './hover-action/hover-action.component';
import { HoverActionPopupComponent } from './hover-action/hover-action-popup.component';
import { MaterialModule } from './material.module';
import { NgModule } from '@angular/core';
import { OverlayModule } from '@angular/cdk/overlay';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    CollectionComponent,
    HoverActionComponent,
    HoverActionPopupComponent,
  ],
  exports: [
    CollectionComponent,
    HoverActionComponent,
    HoverActionPopupComponent,

    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    RouterModule,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    OverlayModule,
  ],
})
export class SharedModule { }
