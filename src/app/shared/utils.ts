import { AppState } from './state.interface';

export const noop = () => { };

export function traverse<T>(object: T, transform: (...x) => T, condition: (...x) => boolean): T {
  return condition(object)
    ? object
    : transform(object)
      ? traverse(transform(object), transform, condition)
      : object;
}

export function decOrder(value: number) {
  return value - 0.005;
}

export function incOrder(value: number) {
  return value + 0.005;
}

export function reorder(data: { order: number }[]) {
  return data.map((item, index) =>
    ({ ...item, order: (index + 1) / 100 }));
}

export function debug(state) {
  const date = new Date();
  const time
    = '['
    + date.getHours() + ':'
    + date.getMinutes() + ':'
    + date.getSeconds() + ':'
    + date.getMilliseconds()
    + ']';
  console.log(time, state);
}

export function sort<T>(direction: 'asc' | 'desc', pick: (x: T) => T[ keyof T ]) {
  return (a, b) => {
    a = pick(a);
    b = pick(b);

    const A = isNaN(+a)
      ? a
      : +a;
    const B = isNaN(+b)
      ? b
      : +b;
    const r = (A < B ? -1 : 1) * (direction.toLowerCase() === 'asc' ? 1 : -1);
    return r;
  };
}

/** Random key generator used by Firebase. */
export function randomKey() {
  // https://gist.github.com/mikelehen/3596a30bd69384624c11

  // Modeled after base64 web-safe chars, but ordered by ASCII.
  const PUSH_CHARS =
    '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';

  // Timestamp of last push, used to prevent local collisions if you push twice in one ms.
  let lastPushTime = 0;

  // We generate 72-bits of randomness which get turned into 12 characters and appended to the
  // timestamp to prevent collisions with other clients.  We store the last characters we
  // generated because in the event of a collision, we'll use those same characters except
  // "incremented" by one.
  const lastRandChars = [];

  /* istanbul ignore next */
  return (function () {
    let now = new Date().getTime();
    const duplicateTime = now === lastPushTime;

    lastPushTime = now;
    const timeStampChars: string[] = new Array(8);

    let i: number;

    for (i = 7; i >= 0; i--) {
      timeStampChars[ i ] = PUSH_CHARS.charAt(now % 64);
      // NOTE: Can't use << here because javascript will convert to int and lose the upper bits.
      now = Math.floor(now / 64);
    }

    if (now !== 0) {
      throw new Error('We should have converted the entire timestamp.');
    }

    let id = timeStampChars.join('');

    if (!duplicateTime) {
      for (i = 0; i < 12; i++) {
        lastRandChars[ i ] = Math.floor(Math.random() * 64);
      }
    } else {
      // If the timestamp hasn't changed since last push, use the same random number, except incremented by 1.
      for (i = 11; i >= 0 && lastRandChars[ i ] === 63; i--) {
        lastRandChars[ i ] = 0;
      }
      lastRandChars[ i ]++;
    }

    for (i = 0; i < 12; i++) {
      id += PUSH_CHARS.charAt(lastRandChars[ i ]);
    }

    if (id.length !== 20) {
      throw new Error('Length should be 20.');
    }

    return id;
  })();
}
