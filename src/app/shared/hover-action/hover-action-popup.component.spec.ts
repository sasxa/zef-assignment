import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { HoverActionPopupComponent } from './hover-action-popup.component';
import { StateService } from '../state.service';

describe('HoverActionPopupComponent', () => {
  let component: HoverActionPopupComponent;
  let fixture: ComponentFixture<HoverActionPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoverActionPopupComponent ],
      providers: [
        { provide: StateService, useValue: null }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoverActionPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
