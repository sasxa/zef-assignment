import { Component, ElementRef, HostListener, OnInit } from '@angular/core';

import { StateService } from '../state.service';

@Component({
  selector: 'hover-action-popup',
  templateUrl: './hover-action-popup.component.html',
  styleUrls: [ './hover-action-popup.component.scss' ]
})
export class HoverActionPopupComponent implements OnInit {
  private timer;

  constructor(
    private eRef: ElementRef,
    public state: StateService) { }

  ngOnInit() {
    this.timer = setTimeout(() => this.close(), 2500);
  }

  private close() {
    this.state.updateUI({ popup: null });
    clearTimeout(this.timer);
  }

  @HostListener('mouseleave', [ '$event' ])
  private onLeave(event: MouseEvent) {
    this.timer = setTimeout(() => this.close(), 1500);
  }

  @HostListener('mouseenter', [ '$event' ])
  private onEnter(event: MouseEvent) {
    clearTimeout(this.timer);
  }

  @HostListener('document:click', [ '$event' ])
  private onClickOutside(event: MouseEvent) {
    if (!event.srcElement.contains(this.eRef.nativeElement)) {
      this.close();
    }
  }
}
