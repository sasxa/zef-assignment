import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { HoverActionComponent } from './hover-action.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { StateService } from '../state.service';

describe('HoverActionComponent', () => {
  let component: HoverActionComponent;
  let fixture: ComponentFixture<HoverActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoverActionComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: StateService, useValue: null }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoverActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
