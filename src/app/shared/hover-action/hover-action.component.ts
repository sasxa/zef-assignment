import { Component, HostListener, Input } from '@angular/core';

import { ConnectionPositionPair } from '@angular/cdk/overlay';
import { StateService } from '../state.service';

@Component({
  selector: 'hover-action',
  templateUrl: './hover-action.component.html',
  styleUrls: [ './hover-action.component.scss' ]
})
export class HoverActionComponent {
  public positions: ConnectionPositionPair[]
    = [ new ConnectionPositionPair(
      { originX: 'end', originY: 'center' },
      { overlayX: 'start', overlayY: 'center' }
    ) ];

  private timer;

  @Input() key: string;

  constructor(public state: StateService) { }

  @HostListener('mouseleave', [ '$event' ])
  private onLeave(event: MouseEvent) {
    clearTimeout(this.timer);
  }

  @HostListener('mouseenter', [ '$event' ])
  private onEnter(event: MouseEvent) {
    this.timer = setTimeout(() => {
      this.state.updateUI({ popup: this.key });
    }, 350);
  }
}
