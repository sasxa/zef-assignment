import { AppPage } from './app.po';

describe('zef-assignment App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getPageTitle()).toEqual('ZEF Assignment');
  });
});
